#!/usr/bin/python -tt
# This is the main file of an entire application
__author__ = "Viet Trinh"  # vqtrinh@ucsc.edu

from unitest import *
from popular import *
from spam_score import client_spam_score
import filelocation
from spike import *


# Unit-test for all modules
def unit_test():
    # Unit-test for cluster popular users
    unittest_cluster.test()

    # Unit-test for determining popular users
    success = unittest_popular.user_test()

    # Unit-test for determining businesses rated by popular users and their trusty-score
    if success:
        unittest_popular.business_test()

    # Unit test for determining businesses' spam scores
    unittest_spam_score.run()


def main():
    print 'Yelp Deceptive Rating\n'

    # Determine a list of popular users, a list of businesses rated by these users, and their trusty scores
    success = user.get_popular_users(filelocation.USER_FILE)
    if success:
        pop_bus, bus_trusty = business.get_business(filelocation.GEN_POP_UID, filelocation.REVIEW_FILE)
        # to view businesses rated by popular users, uncomment the following lines
        # print pop_bus
        # print bus_trusty

    # Determine a spike for each businesses
    spike = spike_detection.rsd(filelocation.REVIEW_FILE)
    print 'Founded: ', len(spike), 'spiky businesses'

    # Determine a spam score for business, review
    client_spam_score.run()

    # Pick up the pickle and add review/business spam scores
    client_spam_score.convert()

    # Quarantine deceptive users
    quarantine.deceptive_users()


# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
    # unit_test()
    main()
