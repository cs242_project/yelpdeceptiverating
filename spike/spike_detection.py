#!/usr/bin/python
# coding=utf-8
# This module detect the spike among Ratings
__author__ = "Samira Zare"  # - szare@ucsc.edu
__author__ = "Shide Homayon"  # - shomayon@ucsc.edu

import csv
import popular.business as pop_bus

REVIEW_THRESHOLD = 10
OUTLIER_OFFSET = 1.5

# Get a list of businesses rated by popular users
def target_business():
    bid_set = set()
    bid_dict = pop_bus.to_dict()

    for uid, bid_list in bid_dict.items():
        for bid in bid_list:
            bid_set.add(bid)
    return bid_set

# Review Spike Detection Algorithm
def rsd(file_path):
    # Read in the first row as column headers
    f = open(file_path)
    reader = csv.reader(f)
    labels = next(reader)
    bid, review_counter, new_business = '0', 0, False
    found_spike = {}

    # Read business_id from each row
    # Calculate all the spike and clean up hanging data list
    pop_bid_set = target_business()
    for row_ptr in reader:
        current_bid = row_ptr[labels.index('business_id')]
        if current_bid in pop_bid_set:
            if bid != current_bid:
                if new_business is True:
                    # Sort the list of stars to calculate spike
                    temp_star_list.sort()

                # Calculate spike if the business has more than 10 reviews
                if review_counter > REVIEW_THRESHOLD:
                    # Calculate spike range by using three quartiles, based on location
                    if (review_counter % 2 == 0):
                        x = review_counter / 2
                        Q2 = (temp_star_list[x - 1] + temp_star_list[x]) / 2
                        if (x % 2 == 0):
                            y = x / 2
                            Q1 = (temp_star_list[y] + temp_star_list[y - 1]) / 2
                            Q3 = (temp_star_list[x + y - 1] + temp_star_list[x + y]) / 2
                        else:
                            y = (x - 1) / 2
                            Q1 = temp_star_list[y]
                            Q3 = temp_star_list[x + y]
                    else:
                        x = (review_counter - 1) / 2
                        Q2 = temp_star_list[x]
                        if (x % 2 == 0):
                            y = x / 2
                            Q1 = (temp_star_list[y] + temp_star_list[y - 1]) / 2
                            Q3 = (temp_star_list[x + y] + temp_star_list[x + y + 1]) / 2
                        else:
                            y = (x - 1) / 2
                            Q1 = temp_star_list[y]
                            Q3 = temp_star_list[x + y + 1]

                    # Specify quartiles' range
                    IQR = Q3 - Q1
                    first_outlier = Q3 + OUTLIER_OFFSET * IQR
                    second_outlier = Q1 - OUTLIER_OFFSET * IQR

                    # User index list for finding user_id and review_id of a spike
                    index_user, idx = [], 0

                    # Check each user rating with the spike range for a business
                    for row in star:
                        if row > first_outlier:
                            flag_spiky_business = True  # spike detected
                            index_user.append(idx)
                        elif row < second_outlier:
                            flag_spiky_business = True  # spike detected
                            index_user.append(idx)
                        idx += 1

                    # What to do when spike is detected
                    spiky_business_list = []
                    if flag_spiky_business == True:
                        spiky_business_list.append(bid)
                        spike_user = []

                        # Add the information of the spiky ratings to the lists
                        for row in index_user:
                            spike_user.append(user[row])
                            spike_star.append(star[row])
                            spike_rids.append(rid[row])

                        # Add each spiky business with its users information to an output dictionary
                        # which will be used as an input to evaluate spiky users
                        spike_values = []
                        for row in range(len(index_user)):
                            spike_dict = dict()
                            spike_dict["user_id"] = spike_user[row]
                            spike_dict["review_id"] = spike_rids[row]
                            spike_dict["stars"] = spike_star[row]
                            spike_values.append(spike_dict)
                        found_spike[bid] = spike_values

                # Reset the new business flag
                new_business = True
                bid = row_ptr[labels.index('business_id')]

                # Reset old spike's information
                date, star, user, rid = [], [], [], []
                spike_star, spike_rids = [], []
                flag_spiky_business = False
                temp_star_list = []  # an unsorted and temporary list of stars for calculating quartiles

                # Add new business's information
                review_counter = 0
                rid.append(row_ptr[labels.index('review_id')])
                date.append(row_ptr[labels.index('date')])
                star.append(row_ptr[labels.index('stars')])
                temp_star_list.append(float(row_ptr[labels.index('stars')]))
                user.append(row_ptr[labels.index('user_id')])
                review_counter += 1

            # What to do when this business is already in the list of spike
            else:
                rid.append(row_ptr[labels.index('review_id')])
                date.append(row_ptr[labels.index('date')])
                star.append(row_ptr[labels.index('stars')])
                temp_star_list.append(float(row_ptr[labels.index('stars')]))
                user.append(row_ptr[labels.index('user_id')])
                review_counter += 1

    f.close()
    return found_spike
