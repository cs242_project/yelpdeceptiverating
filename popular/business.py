#!/usr/bin/python -tt
# This module extracts a set of business rated by popular users from Yelp Challenge DataSet
__author__ = "Viet Trinh"  # vqtrinh@ucsc.edu

import pandas as pd
import numpy as np
import os.path, filelocation

useful_cols = ['user_id', 'business_id', 'stars']


# Input is a list of popular users and review dataset
# Output is a list of businesses rated by these users and their trusty-score
def get_business(popular_user_list, review_file_path):
    print '---------- DETERMINE BUSINESSES RATED BY POPULAR USERS ----------'
    fptr = None
    if os.path.exists(review_file_path):
        file_name = review_file_path.split('/')[-1:]
        print 'Reading file', file_name[0]
        fptr = pd.read_csv(review_file_path, usecols=useful_cols)
    else:
        print 'Error:', file_name[0], ' does not exist. Please check the file path'
        return

    # create a set of distinct popular users
    print 'Create a list of popular users based on uids'
    popular_uids = set([line.rstrip('\n') for line in open(popular_user_list)])

    # dictionary contains set of business rated by an user
    # { user1 : set([bus_1, bus_2, ...]),
    #   user2 : set([bus_3, bus_2, ...]),
    #   user3 : set([bus_5, bus_8, ...]),
    #   ...}
    usr_bus_dict = {}

    # dictionary contains a set of rating for a business
    # that does not include popular users' rating
    # { bus_1 : [2.0, 4.5, ...],
    #   bus_2 : [4.3, 3.6, ...],
    #   bus_3 : [5.0, 5.0, ...],
    #   ...}
    bus_rate_dict = {}

    print 'Extracting businesses rated by popular users ...'
    groupdf = fptr.groupby('user_id')
    for uid, group in groupdf.groups.iteritems():
        group_data = fptr.ix[group]
        for idx in group_data.index:
            print '  review_', idx, ', user_', uid
            row = group_data.loc[idx,]
            bid, rate = row['business_id'], row['stars']
            if uid in popular_uids:
                if uid in usr_bus_dict:
                    usr_bus_dict[uid].add(bid)
                else:
                    usr_bus_dict[uid] = set([bid])
            else:
                if bid in bus_rate_dict:
                    bus_rate_dict[bid] = np.concatenate((bus_rate_dict[bid], np.array([rate])), axis=0)
                else:
                    bus_rate_dict[bid] = np.array([rate])
    print 'Extracting businesses rated by popular users: DONE'

    print 'Calculating trusted rating for each business ...',
    for bid, rates in bus_rate_dict.items():
        bus_rate_dict[bid] = np.mean(rates)
    print 'DONE'

    save_bid_to_file(usr_bus_dict)
    save_trusty_to_file(bus_rate_dict)

    return [usr_bus_dict, bus_rate_dict]


# Save all founded businesses and their rated popular users into a text file
def save_bid_to_file(usr_bus_dict):
    print 'Writing out popular users and their rated businesses ...',
    ''' Format of the output file:
            user_id_1,[bus_id_1,bus_id_2,bus_id_3,...]
            user_id_2,[bus_id_2,bus_id_4,...]
            user_id_3,[bus_id_1,bus_id_3,...]
            ... '''
    of_stream = open('./gen/gen_pop_bid.csv', 'wb')
    of_stream.write('user_id,business_list' + '\n')
    for uid, bid_list in usr_bus_dict.items():
        content = uid + ',\"['
        for bid in bid_list:
            content += bid + ','
        content += ']\"'
        of_stream.write(content + '\n')
    of_stream.close()
    print 'DONE'


# Save all founded businesses and their trusty score into a text file
def save_trusty_to_file(bus_rate_dict):
    print 'Writing out businesses and their trusty score ...',
    ''' Format of the output file:
            bus_id_1,trusted_rate_1
            bus_id_2,trusted_rate_2
            bus_id_3,trusted_rate_3
            ... '''
    of_stream = open('./gen/gen_bid_trusty.csv', 'wb')
    of_stream.write('business_id,trusted_rate' + '\n')
    for bid, score in bus_rate_dict.items():
        of_stream.write(bid + ',' + str(score) + '\n')
    of_stream.close()
    print 'DONE'


# Convert csv into a dictionary for businesses rated by popular users
def to_dict():
    pop_bid_dict = {}
    gen_fptr = pd.read_csv(filelocation.GEN_POP_BID, usecols=['user_id', 'business_list'])
    for idx in gen_fptr.index:
        row = gen_fptr.loc[idx,]
        uid = row['user_id']
        bid_list = row['business_list'].split(',')
        bid_list[0] = bid_list[0][1:]
        pop_bid_dict[uid] = bid_list[:-1]
    return pop_bid_dict
