#!/usr/bin/python -tt
# coding=utf-8
# This module quarantines popular users based on calculated BRN score
__author__ = "Viet Trinh"  # vqtrinh@ucsc.edu

import pickle, filelocation
import pandas as pd

TRUSTED_THRESHOLD = 0.5
MAX_DECEPT_RATING = 10


# Read in found_spike dictionary, popular users, businesses and their trusty score
# from generated outputs of previous modules
def files_reader():
    print 'Reading pickle files...',
    fptr = open(filelocation.GEN_RSD_BID, 'rb')
    brn_bid_dict = pickle.load(fptr)
    fptr.close()

    fptr = open(filelocation.GEN_RSD_RID, 'rb')
    brn_rid_dict = pickle.load(fptr)
    fptr.close()

    bid_trusty_dict = {}
    fptr = pd.read_csv(filelocation.GEN_BID_TRUSTY, usecols=['business_id', 'trusted_rate'])
    for idx in fptr.index:
        row = fptr.loc[idx,]
        bid = row['business_id']
        bid_trusty_dict[bid] = row['trusted_rate']

    pop_uid_set = set()
    fptr = pd.read_csv(filelocation.GEN_POP_UID, usecols=['user_id'])
    for idx in fptr.index:
        row = fptr.loc[idx,]
        pop_uid_set.add(row['user_id'])

    print 'DONE'

    return bid_trusty_dict, brn_bid_dict, brn_rid_dict, pop_uid_set


# Determine deceptive users based on a threshold number of unreliable ratings
# made on target businesses
def deceptive_users():
    bid_trusty_dict, brn_bid_dict, brn_rid_dict, pop_uid_set = files_reader()
    uid_decept_rid_count, uid_decept_set = {}, set()

    print 'Determine deceptive users...'
    for bid, bscore in brn_bid_dict.items():
        lb, ub = bid_trusty_dict[bid] - TRUSTED_THRESHOLD, bid_trusty_dict[bid] + TRUSTED_THRESHOLD
        # if business spam score is out of tolerate range
        if bscore < lb or bscore > ub:
            review_dict_list = brn_rid_dict[bid]
            for review_dict in review_dict_list:
                rscore = review_dict['review_score']
                uid = review_dict['user_id']
                # if a user is popular and the rating is out of tolerate range
                if uid in pop_uid_set:
                    if rscore < lb or rscore > ub:
                        if uid in uid_decept_rid_count:
                            uid_decept_rid_count[uid] += 1
                        else:
                            uid_decept_rid_count[uid] = 1

    # Build a set of distinct and deceptive popular users
    for uid, count in uid_decept_rid_count.items():
        if count >= MAX_DECEPT_RATING:
            uid_decept_set.add(uid)

    # Output statistics
    print 'Founded ', len(uid_decept_set), ' deceptive popular users'
    print 'Percentage Population: %0.02f%%' % (len(uid_decept_set) * 100. / len(pop_uid_set))
