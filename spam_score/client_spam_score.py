#!/usr/bin/python3
# coding=utf-8
"""
This module runs spam score generator on the entire dataset
"""
__author__ = "Vikrant More" # hit me up - vmore@ucsc.edu

from spam_score import spam_score_gen as ss
import filelocation
import numpy as np
import time
import csv
import pickle

def run():
    start_time = time.time()
    review_csv_file = filelocation.REVIEW_FILE
    num_rows = None # Choose all rows

    print("Loading the dataset..\n")
    reviews = ss.pull_data_csv(review_csv_file, num_rows)

    print("Loaded the dataset now extracting some generic features..\n")
    business_id, business_review_sum, business_review_count, review_id = ss.generic_data(reviews)

    print("Extracting row iteration features, most are review features, one business feature..\n")
    RD, EXT, ETF, ISR, EXC, PP1, PC, RL, business_id = ss.extract_review_features(reviews, business_id, business_review_sum, business_review_count)

    print("Extracted row iteration features, now extracting mostly business features..\n")
    MNR, PR, NR, avgRD, BST, ERD, ETG = ss.extract_business_features(reviews, RD, business_id, business_review_count)

    # RD, EXT, ETF, ISR, EXC, PP1, PC <--order. following poles are in this order
    review_feature_poles = [1, 1, 1, 1, 1, 0, 1]
    review_feature_vector = np.zeros((len(review_id), len(review_feature_poles)))

    feature_index = list(range(len(review_feature_poles)))
    feature_list = [RD, EXT, ETF, ISR, EXC, PP1, PC]

    print("Extracted all features, now generating the review feature vector. Its size is {}x{}\n".format(len(review_id), len(review_feature_poles)))
    # construct feature vector for reviews
    for i, id in enumerate(review_id):
        for j in feature_index:
            review_feature_vector[i][j] = feature_list[j][id]

    # MNR - H, PR - H, NR - H, avgRD - H, BST - H, ERD - L, ETG - L, RL - L,  <--order. following poles are in this order
    business_feature_poles = [1, 1, 1, 1, 1, 0, 0, 0]
    business_feature_vector = np.zeros((len(business_id), len(business_feature_poles)))

    bid = dict()
    business_id = list(business_id)
    for i, id in enumerate(business_id):
        bid[id] = i

    business_feature_index = list(range(len(business_feature_poles)))
    business_feature_list = [MNR, PR, NR, avgRD, BST, ERD, ETG, RL]

    print("Now generating the business feature vector. Its size is {}x{}\n".format(len(business_id), len(review_feature_poles)))
    # construct feature vector for reviews
    for id in business_id:
        idx = bid[id]
        for j in business_feature_index:
            business_feature_vector[idx][j] = business_feature_list[j][id]

    # compute spam score
    print("Computing the spam score..\n")
    review_score = ss.compute_spam_score(review_feature_poles, review_feature_vector)
    business_score = ss.compute_spam_score(business_feature_poles, business_feature_vector)

    print("Saving to gen/review_spam_score.csv and gen/business_spam_score.csv")
    for k, v in bid.items():
        bid[k] = business_score[bid[k]]

    with open(filelocation.GEN_SS_RID, 'w') as csv_file:
        writer = csv.writer(csv_file)
        for i, id in enumerate(review_id):
            writer.writerow([id, review_score[i]])

    with open(filelocation.GEN_SS_BID, 'w') as csv_file:
      writer = csv.writer(csv_file)
      for key, value in bid.items():
          writer.writerow([key, value])

    print("--- Finished in {0:0.2f} minutes ---\n".format((time.time() - start_time)/60))

def convert():
    with open(filelocation.GEN_RSD_OP, "rb") as handle:
        rsd_op = pickle.load(handle)

    with open(filelocation.GEN_SS_BID, 'r') as csv_file:
        reader = csv.reader(csv_file)
        business_score = dict(reader)

    for k, v in business_score.items():
        business_score[k] = float(v)

    pop_bus_score = dict()

    for k, v in rsd_op.items():
        pop_bus_score[k] = business_score[k]

    with open(filelocation.GEN_SS_RID, 'r') as csv_file:
        reader = csv.reader(csv_file)
        review_score = dict(reader)

    for k, v in review_score.items():
        review_score[k] = float(v)

    for k, v in rsd_op.items():
        for items in v:
            items["review_score"] = review_score[items["review_id"]]

    with open(filelocation.GEN_RSD_BID, 'wb') as handle:
        pickle.dump(pop_bus_score, handle, protocol=2)

    with open(filelocation.GEN_RSD_RID, 'wb') as handle:
        pickle.dump(rsd_op, handle, protocol=2)
