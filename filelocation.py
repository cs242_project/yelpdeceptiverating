# Absolute location of data files
SAMPLE_USER_FILE = './dataset/sample_user.csv'
SAMPLE_REVIEW_FILE = './dataset/sample_review.csv'
USER_FILE = './dataset/yelp_academic_dataset_user.csv'
REVIEW_FILE = './dataset/yelp_academic_dataset_review.csv'

# Relative location of generated output files
GEN_SAMPLE_POP_UID = './gen/sample_pop_uid.csv'
GEN_POP_UID = './gen/gen_pop_uid.csv'
GEN_POP_BID = './gen/gen_pop_bid.csv'
GEN_BID_TRUSTY = './gen/gen_bid_trusty.csv'
GEN_RSD_BID = './gen/gen_rsd_bid_brn.pickle'
GEN_RSD_RID = './gen/gen_rsd_rid_brn.pickle'
GEN_RSD_OP = './gen/gen_rsd_op.pickle'
GEN_SS_BID = './gen/business_spam_score.csv'
GEN_SS_RID = './gen/review_spam_score.csv'
