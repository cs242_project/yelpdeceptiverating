import unittest
from spike import *
import filelocation

class RSDTest(unittest.TestCase):
    manual_dictionary = {
        '6ilJq_05xRgek_8qUp36-g': {'User_Star4': '4', 'User_Id0': '-V4cUmLyjk6Cv3hS0jw3hw', 'User_Star5': '4',
                                   'User_Review_ID5': 'SNmNLFCfIcclAMnpD5fKYA',
                                   'User_Review_ID2': 'MYrWcO00L2tk2g4iri_17Q',
                                   'User_Review_ID1': 'CtxqLHFB80H64X62p_N0Bw', 'User_Id4': 'Q3fFv_ft17OyV-NRF1iQxw',
                                   'User_Review_ID0': 'AMNg45U8jRzwPENj5QmDSw', 'User_Id2': 'PwANTxC5mSWGVeSvU1zbYg',
                                   'User_Star2': '4', 'User_Review_ID3': 'BysFiyYjT0pe3zYS0zSFrQ',
                                   'User_Id3': '-OUKIatcSXuF-40IHcarwA', 'User_Star0': '4',
                                   'User_Review_ID4': 'DGGmV1P0vOGgp3kpRd6p-A', 'User_Star3': '4',
                                   'User_Id1': 'WMTm9HHRA3EewoxTX1Gleg', 'User_Id5': 'q2ccFmRoK0lkkRPosgKFFg',
                                   'User_Star1': '4'},
        'ISKLP0qxApATxRlUDob6Cg': {'User_Id0': 'KTlXR4E_JJdgBSPpiO4Oig', 'User_Star0': '1',
                                   'User_Review_ID0': '30pAoD-y1GdxvVo3J56sUg'},
        'eT5Ck7Gg1dBJobca9VFovw': {'User_Star2': '1', 'User_Review_ID1': '5uZ8_StpG-E7ck9kIiOuwA', 'User_Star0': '1',
                                   'User_Review_ID2': 'PL5tiN19vPHpLJK_3PmOPw', 'User_Id1': 'RP4kX4YZDvakqSF25tiuEA',
                                   'User_Id0': 'S3wBsjAWlUo61ggMeErjHw', 'User_Review_ID0': 'SG3U-z5wSEWVAsikuuREDw',
                                   'User_Star1': '1', 'User_Id2': 'ljGxYz9as2xIvojzh-5Row'},
        'b9WZJp5L1RZr4F1nxclOoQ': {'User_Star4': '1', 'User_Id0': 'S5oBX7uhY4wYyDjvka4cWg', 'User_Star5': '1',
                                   'User_Review_ID5': 'MzsMhW2KOydoyMXWXXKzWg',
                                   'User_Review_ID2': 'TdGpzgCt97O82UVBzOn_FQ',
                                   'User_Review_ID1': 'm2CjXug2YN1_tDEPmAmogQ', 'User_Id4': 'AaXxndU7QoMwU9NptjMhSg',
                                   'User_Review_ID0': 'SxaE8AKzEfYk-6SU3_LZhw', 'User_Id2': 'Ft4NZwOB9xQJkRwuvsDO-A',
                                   'User_Star2': '1', 'User_Review_ID3': 'HvzazIqdwWcSVs0JFm9bcg',
                                   'User_Id3': 'yB0v8l4NMWJDeaq2h2mgUg', 'User_Star0': '2',
                                   'User_Review_ID4': 'eIZMWBIKI0JSbsUYT3_SFw', 'User_Star3': '1',
                                   'User_Id1': 'Ei7ootM9QUDHeB1OJIkfIw', 'User_Id5': 'xr3YPIcYkGHZZ1lSnJpDKQ',
                                   'User_Star1': '1'},
        'tv8cS4aaA1VDaInYgggb6g': {'User_Review_ID1': 'lwcIJiMwk2JPpd9fgeJz7Q', 'User_Star0': '1',
                                   'User_Id1': '5SLDX3bXcRP52QlStIcYLg', 'User_Id0': '2_ZX4VQU32gF9Dhi0PhQFw',
                                   'User_Review_ID0': 'YD0dLWnTqCgM0SwKB2aoCA', 'User_Star1': '1'},
        '0hrB2iwQZ52VZcZWYywXrA': {'User_Star2': '1', 'User_Review_ID1': 'VWpgnzRyBzo8-ZXQyx0jzg', 'User_Star0': '2',
                                   'User_Review_ID2': '4TXPBkkQkR3Oxc_Z0PmvjQ', 'User_Id1': '5-4LQRi8LoGonuATXEE5TQ',
                                   'User_Id0': '8kyyeRdryG9mbW0i7vCkgQ', 'User_Review_ID0': 'soh3WppI_zMdNmiIH_52Hg',
                                   'User_Star1': '1', 'User_Id2': '5-4LQRi8LoGonuATXEE5TQ'}}

    def test_checking_RSD(self):
        self.assertEqual(spike_detection.rsd(filelocation.SAMPLE_REVIEW_FILE), self.manual_dictionary)