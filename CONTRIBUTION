Project: Quarantine deceiving Yelp's users by detecting unreliable rating reviews
Course: CMPS-242 Machine Learning
Quarter: Fall 2016
=====================================================================================

*** TEAM MEMBER ***
Viet Trinh - vqtrinh@ucsc.edu
Vikrant More - vmore@ucsc.edu
Samira Zare - szare@ucsc.edu
Sheideh Homayon - shomayon@ucsc.edu

*** SOURCE CODE REPOSITORY ***
https://bitbucket.org/cs242_project/yelpdeceptiverating

*** WORKLOAD CONTRIBUTION ***
This project receives an equal contribution from all the team members,
in terms of idea brainstorming, project development, coding implementation,
and report writing. More specifically:

1. Viet Trinh: 
	Source Code Implementation: cluster popular users and businesses, calculate
	                            trusty scores, and determine deceptive users
	Written Report: Abstract, Introduction, Feature Engineering - Popular Users,
	                Detecting Anamoly in Ratings - Clustering Popular Users,
	                Evaluation, and Results

2. Vikrant More: 
	Source Code Implementation: calculate business spam score and review spam score,
	                            unit test for different modules, and output generations
	Written Report: Feature Engineering - Potential Fraudulent Reviews and Target Spamming
	                Businesses, Detecting Anamoly in Ratings - Bridging Networks for
	                Calculating Spam Scores, Evaluation, and Results
	
3. Samira Zare: 
	Source Code Implementation: RSD for positive reviews, unit test for RSD, input
	                            read-in and output generations
	Written Report: Detecting Anamoly in Ratings - Review Spike Detection (RSD),
	                Evaluation, and Results
	
4. Sheideh Homayon: 
	Source Code Implementation: RSD for negative reviews, unit test for RSD, input
	                            read-in and output generations
	Written Report: Detecting Anamoly in Ratings - Review Spike Detection (RSD),
	                Evaluation, and Results